<?php
//insert new row into table with parameters set on $params_string
function getAnkontrBlogs(mysqli $db_connect){
    $result=mysqli_query($db_connect,"SELECT pr_news.n_id, pr_news.n_title title, pr_news.n_fulltext description, pr_user.name authorname, pr_news.n_date adddate FROM `pr_news` inner join pr_user ON pr_news.author_id=pr_user.id WHERE is_new=2 order by n_id asc");
    return $result;
}
function getAnkontrBlogAuthors(mysqli $db_connect){
    $result=mysqli_query($db_connect,"SELECT pr_user.name authorname FROM `pr_news` inner join pr_user ON pr_news.author_id=pr_user.id WHERE is_new=2 group by pr_news.author_id");
    return $result;
}
function getAuthorIdByName(mysqli $db_connect,$name){
    $result=mysqli_query($db_connect,"SELECT * FROM blogs_type WHERE name='".$name."'");
    while($row = mysqli_fetch_assoc($result))
        {
           return $row['id'];
        }
}
function addBlogsType(mysqli $db_connect, $params_array){
    $result=mysqli_query($db_connect,"INSERT INTO `blogs_type` VALUES (DEFAULT,
      '".$params_array['authorname']."', '');");
}
function addBlogs(mysqli $db_connect, $params_array){
    $result=mysqli_query($db_connect,"INSERT INTO `blogs` VALUES (DEFAULT,
      '".$params_array['title']."', '".$params_array['description']."',
      '7','".$params_array['typeid']."','".$params_array['adddate']."',
      '1');");
}
function getAnkontrAnns(mysqli $db_connect){
  $result=mysqli_query($db_connect,"SELECT pr_board.id,pr_board.title, pr_board.text, pr_board.publish_type, pr_board.phone, pr_board.added, pr_board_cat.name catname, pr_board_cat.p_id FROM `pr_board` inner join pr_board_cat on pr_board_cat.id=pr_board.c_id WHERE expired>".time()." and deleted=0 and phone<>'NULL' and publish_type>-1 and publish_type<3 ");
  return $result;
}
function getAnkontrAnnsFiles(mysqli $db_connect,$id){
  $result=mysqli_query($db_connect,"SELECT * FROM `pr_board_img` WHERE b_id=".$id);
  if ($result==false){
    return false;
  } else {
  return $result;
}
}
function getCategoryIdByName(mysqli $db_connect,$name,$pid){
    $result=mysqli_query($db_connect,"SELECT * FROM ann_categories WHERE categoryname='".$name."' AND parentid=".$pid);
    if ($result==false){
      return false;
    } else {
    while($row = mysqli_fetch_assoc($result))
        {
           return $row['id'];

        }
      }
}
function addAnnCategory(mysqli $db_connect, $catname,$pid){
    $result=mysqli_query($db_connect,"INSERT INTO `ann_categories` VALUES (DEFAULT,
      '".$pid."', '".$catname."',
      '1', '1');");
      return mysqli_insert_id($db_connect);
}
function addAnnouncement(mysqli $db_connect, $params_array){
    $result=mysqli_query($db_connect,"INSERT INTO `announcements` VALUES (DEFAULT,
      '1', '".$params_array['categoryid']."', '".$params_array['title']."',
      '".$params_array['typeid']."','".$params_array['text']."',
      '".$params_array['phone']."', '".$params_array['adddatetime']."','1','1');");
}
function addAnnFile(mysqli $db_connect, $params_array){
    $result=mysqli_query($db_connect,"INSERT INTO `ann_files` VALUES (DEFAULT,
      '".$params_array['annid']."', '".$params_array['filepath']."');");
}
function getUserIdByEmail(mysqli $db_connect,$mail){
    $result=mysqli_query($db_connect,"SELECT * FROM users WHERE email='".$mail."'");
    if ($result==false){
      return false;
    } else {
    while($row = mysqli_fetch_assoc($result))
        {
           return $row['id'];

        }
      }
}
function addUser(mysqli $db_connect, $params_array){
  $result=mysqli_query($db_connect,"INSERT INTO `users` VALUES (DEFAULT,
    '".$params_array['login']."','".md5($params_array['password'])."',
    '".$params_array['email']."', NOW(),
    '4','0','');");
    return mysqli_insert_id($db_connect);
}
function getAnkontrUsers(mysqli $db_connect){
    $result=mysqli_query($db_connect,"SELECT pr_user.id, pr_user.name login, pr_user.email, pr_user.password FROM `pr_user`  WHERE active=1");
    return $result;
}
function getAnkontrPaymentsByUser(mysqli $db_connect,$id){
    $result=mysqli_query($db_connect,"SELECT amount, FROM `pr_user`  WHERE active=1");
    return $result;
}
function addUserBalance(mysqli $db_connect, $params_array){
    $result=mysqli_query($db_connect,"INSERT INTO `userbalance` VALUES (DEFAULT,
      '".$params_array['userid']."', '".$params_array['amount']."', NOW());");
}
?>
