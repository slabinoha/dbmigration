Configuration process:

For Ankontr:
1. Set corresponding values for origin and destination database connections in origindb and destinationdb files.
2. Run script ankontrmigration.php
3. Set photos for blog types
4. Set proper parent categories for generated categories
5. Move files from ancontr to uploads/oldimages directory.
